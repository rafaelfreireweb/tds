import React from 'react'
import './styles.scss';

import ManifestosComponent from '../../components/ManifestosComponent';

import logo from '../../assets/img/logo-horizontal.png';


export default function Home() {
  
  return (
    <>
      <div className="intro hide"> 
        <img src={logo} alt="The Digital Strategy" className="intro-logo" />  
        <div className="intro-text">
          <p>Construímos</p>
          <p><b>futuros</b> <span>digitais.</span></p>
        </div>
      </div>
      <ManifestosComponent />
    </>
  )
  
}