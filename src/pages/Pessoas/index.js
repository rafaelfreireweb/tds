import React, { useEffect, useState} from 'react'
import api from '../../services/api';
import "./styles.scss";

import logo from '../../assets/img/logo.png';


export default function Pessoas() {

  const [ pessoas, setPessoas] = useState([]);

  useEffect(()=>{
    async function loadPessoas() {
      const response = await api.get('/pages?slug=pessoas');
      setPessoas(response.data);
    }
    loadPessoas();
  }, []);
  

  return (
    pessoas.map((pessoa, index) => (
      
      <div key={index} className="pessoas">
        <div className="pessoas-info content full-screen">
          <div className="pessoas-container">
            <figure className="pessoas-img "> 
              <img src={logo} alt="The Digital Strategy" />
            </figure>
            <div className="pessoas-content" dangerouslySetInnerHTML={{__html: pessoa.content.rendered}}></div>
          </div>
        </div>

        <div className="pessoas-boss full-screen content silvio">
          <div className="pessoas-boss-info">
            <h2>Silvio Meira</h2>
            <p>founder &amp; chief scientist</p>
          </div>
        </div>

        <div className="pessoas-boss full-screen content sergio ">
          <div className="pessoas-boss-info">
            <h2>Sergio Monteiro Cavalcanti</h2>
            <p>founder &amp; managing partner</p>
          </div>
        </div>

        <div className="pessoas-our full-screen">
          <div className="pessoas-our-container">
            <h2>Nossa <b>gente</b></h2>
            <div className="pessoas-our-list">
              {pessoa.acf.team.map((team, index) => (
                <div key={index} className="pessoas-our-item">
                  <div className="avatar">
                    <img src={team.imagem.sizes.thumbnail} alt="" />
                  </div>
                  <p className="name">{team.nome}</p>
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="pessoas-contact grid full-screen">
      
          <div className="pessoas-contact-logo col-8"> 
            <img src="https://s31463.pcdn.co/wp-content/themes/tds/assets/img/logo-white.png" alt="The Digital Strategy" />
          </div>
          
          <div className="pessoas-contact-content col-4">
            <div className="pessoas-contact-content-container">
              <p>
                <strong>Email</strong><br/>
                <a href="mailto:contato@tds.com.pe">contato@tds.com.pe</a>
              </p>
              <p>
                <strong>Telefone</strong><br/>
                            +55 (81) 3032-0222
                        </p>
              <p>
                <strong>Endereço</strong><br/>
                Rua da Guia, 217<br/>
                PORTO DIGITAL<br/>
                RECIFE - BRASIL
              </p>
            </div>
          </div>

        </div>

      </div>
      ))

  )
}