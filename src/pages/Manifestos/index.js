import React, { useEffect, useState} from 'react'
import api from '../../services/api';
import "./styles.scss";

export default function Manifestos({match}) {

  const [ transform, setTransform] = useState([]);

  useEffect(()=>{
    async function loadTransform() {
    const response = await api.get(`/manifestos?slug=${match.params.slug}`);
      setTransform(response.data);
    }
    loadTransform();
  }, [match.params.slug]);
  

  return (
    <div className="manifesto">
      {transform.map((manifesto, index) => (

        <div key={index}>
          <h1>{manifesto.title.rendered}</h1>
        </div>

      ))}
    </div>
  )
}