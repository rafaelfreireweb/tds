import React, { useEffect, useState} from 'react'
import apiAcf from '../../services/apiAcf';
import "./styles.scss";

export default function Transformacao() {

  const [ transform, setTransform] = useState([]);

  useEffect(()=>{
    async function loadTransform() {
      const response = await apiAcf.get('/pages/165');
      setTransform(response.data.acf.conteudo_nosso);
    }
    loadTransform();
  }, []);
  

  return (
    <div className="metodologia">
      {transform.map((transformation, index) => (

        <div key={index} className={`metodologia-container ${transformation.tema_nosso} all `}>
          {transformation.titulo_nosso && (
            <h2 className="metodologia-title">{transformation.titulo_nosso}</h2> 
          )}
          {transformation.sub_titulo_nosso && (
            <h3 className="metodologia-subtitle wow fadeIn">{transformation.sub_titulo_nosso}</h3> 
          )}
          {transformation.texto_nosso && (
            <div className="metodologia-text" dangerouslySetInnerHTML={{__html: transformation.texto_nosso}}></div>
          )}
          {transformation.imagem_nosso && (
            <figure className="metodologia-img wow fadeIn">
              <img src={transformation.imagem_nosso.url} alt={transformation.imagem_nosso.alt}/>
            </figure>
          )}
        </div>

      ))}
    </div>
  )
}