import axios from 'axios';

const apiAcf = axios.create({
  baseURL: 'https://www.tds.com.pe/wp-json/acf/v3'
});

export default apiAcf ;
