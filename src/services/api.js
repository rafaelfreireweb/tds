import axios from 'axios';

const api = axios.create({
  baseURL: 'https://www.tds.com.pe/wp-json/wp/v2'
});

export default api;
