import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Transformacao from './pages/Transformacao';
import Pessoas from './pages/Pessoas';
import Manifestos from './pages/Manifestos';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/transformacao" exact component={Transformacao} />
        <Route path="/pessoas" exact component={Pessoas} />
        <Route path="/manifestos/:slug" exact component={Manifestos} />
      </Switch>
    </BrowserRouter>
  )
} 