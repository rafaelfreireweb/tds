import React, { useEffect } from 'react';
import Routes from './routes';
import './App.scss';

function App() {

  useEffect(() => {
    // Load Page
    setTimeout(function(){
      document.body.classList.add('loading-page'); 
    },1000);
    setTimeout(function(){
      document.body.classList.add('loaded-page'); 
    },4000);
  });

  return (
    <Routes />
  );
}

export default App;
