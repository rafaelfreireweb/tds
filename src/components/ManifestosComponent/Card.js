import React from 'react'
import { Link } from "react-router-dom";
import './styles.scss';

export default function Card(props) {
  return(
    <Link to={props.link}>
      <div className={`js-link-manifest manifest-card manifest-card-${props.slug}`}>
        <div className="manifest-card-info">
          <h2 className="manifest-card-title">{props.title}</h2>
          <div className="manifest-card-text" dangerouslySetInnerHTML={{__html: props.excerpt}}></div>
        </div>
      </div>
    </Link>
  );
} 