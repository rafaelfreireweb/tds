import React, { useEffect, useState} from 'react'
import api from '../../services/api';
import OwlCarousel from 'react-owl-carousel2';
import Card from './Card';
import "../../assets/scss/vendor/owlcarousel/owl.carousel.scss";
import "../../assets/scss/vendor/owlcarousel/owl.theme.default.scss";


export default function ManifestosComponent() {
  
  const options = {
    items: 3,
    loop: false,
    nav: true,
    autoplay: false,
    dots: false
  };
 
  const [ manifests, setManifests] = useState([]);
  useEffect(()=>{
    async function loadManifests() {
      const response = await api.get('/manifestos');
      setManifests(response.data);
    }
    loadManifests();
  }, []);

  return(
    <div className="manifest">
      <OwlCarousel options={options}>
        <div>
          <Card link="transformacao" slug="metodologia" title="Trasnformação" excerpt="<p>Criamos, desenvolvemos e operamos plataformas digitais conectadas</p>" />
        </div>
        <div>
          <Card link="pessoas" slug="pessoas" title="Pessoas" excerpt="<p>Inovamos a partir de pessoas, com pessoas e para pessoas, o tempo todo</p>" />
        </div>
        {manifests.map(manifest => (
          <div key={manifest.id}>
            <Card link={`manifestos/${manifest.slug}`} slug={manifest.slug} title={manifest.title.rendered} excerpt={manifest.excerpt.rendered} />
          </div>
        ))}
      </OwlCarousel>

    </div>
  );

}